package com.mcranked.mgcore.listeners;

import com.mcranked.mgcore.MGCore;
import com.mcranked.mgcore.api.Minigame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionHandling implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Minigame minigame = MGCore.getInstance().getMinigame();
        if (!minigame.isDedicated()) {
            return;
        }
        // automatically add the player to the minigame
        minigame.join(event.getPlayer());
        // remove the join message
        event.setJoinMessage("");
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Minigame minigame = MGCore.getInstance().getMinigame();
        // quit the game
        minigame.quit(event.getPlayer());
        // remove the quit message
        event.setQuitMessage("");
    }
}
