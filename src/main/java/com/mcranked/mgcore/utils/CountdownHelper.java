package com.mcranked.mgcore.utils;

import com.mcranked.mgcore.MGCore;
import com.mcranked.mgcore.api.Minigame;
import lombok.Getter;

public class CountdownHelper implements Runnable {
    @Getter private final Minigame minigame;
    @Getter private final int from;
    @Getter private int seconds;

    public CountdownHelper(Minigame minigame, int from) {
        this.minigame = minigame;
        this.from = from;
        this.seconds = from;
    }

    @Override
    public void run() {
        // make sure second doesn't go below 0
        if (seconds <= 0) {
            // begin the game and cancel this
            minigame.gameStart();
            minigame.cancelCountdown();
            return;
        }
        // send the message notifying all in the game
        String countdownMessage = String.format(MGCore.getConf().getString("gameCountdown.message"), seconds);
        minigame.messageAll(countdownMessage);
        // decrement the current seconds
        --seconds;
    }
}
