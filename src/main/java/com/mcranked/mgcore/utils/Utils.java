package com.mcranked.mgcore.utils;

import com.mcranked.mgcore.MGCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Utils {
    public static String color(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public static void tickDelay(Runnable runnable) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(MGCore.getInstance(), runnable, 1L);
    }
}
