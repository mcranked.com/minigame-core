package com.mcranked.mgcore;

import com.mcranked.mgcore.api.Minigame;
import com.mcranked.mgcore.command.StartCmd;
import com.mcranked.mgcore.listeners.ConnectionHandling;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MGCore extends JavaPlugin {
    @Getter private static MGCore instance;
    @Getter private Minigame minigame = null;

    @Override
    public void onEnable() {
        instance = this;

        getLogger().info("Core loaded successfully!");

        regConfig();
        regCommands();
        regEvents();

        // set up the minigame tick event
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            if (instance.minigame == null) {
                return;
            }
            // call the proper tick method
            if (instance.minigame.isGameStarted()) instance.minigame.onGameTick();
            else instance.minigame.onPreGameTick();
        }, 1L, 1L);
    }

    @Override
    public void onDisable() {
        if (minigame == null || !minigame.isDedicated()) return;

        for (Player player : Bukkit.getOnlinePlayers())
            player.kickPlayer("The server is restarting.");
    }

    public void hook(Minigame minigame) {
        // log that a minigame has been hooked
        getLogger().info(String.format("The minigame known as %s has hooked itself into the Core!", minigame.getName()));

        if (this.minigame != null) {
            // call the disable method for the previous
            // minigame (why there was one loaded, I don't know)
            this.minigame.onDisable();
        }

        // set the new minigame and enable it
        this.minigame = minigame;
        this.minigame.onEnable();
    }

    public static FileConfiguration getConf() {
        return instance.getConfig();
    }

    private void regConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private void regCommands() {
        getCommand("start").setExecutor(new StartCmd());
    }

    private void regEvents() {
        PluginManager manager = Bukkit.getPluginManager();

        manager.registerEvents(new ConnectionHandling(), this);
    }
}
