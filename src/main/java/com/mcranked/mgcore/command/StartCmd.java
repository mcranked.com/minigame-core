package com.mcranked.mgcore.command;

import com.mcranked.mgcore.MGCore;
import com.mcranked.mgcore.api.Minigame;
import com.mcranked.mgcore.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StartCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Minigame minigame = MGCore.getInstance().getMinigame();

        // make sure the game exists
        if (minigame == null) {
            sender.sendMessage(Utils.color("&cThis server has to have a Minigame on it."));
            return true;
        }

        // there cannot be only one person
        if (minigame.getPlayers().size() <= 1) {
            sender.sendMessage(Utils.color("&cYou can't start a game with only one person in it!"));
            return true;
        }

        // check that the game is not already running
        if (minigame.isGameStarted() || minigame.getCountdownHelper() != null) {
            sender.sendMessage(Utils.color("&cHuh? You know that the game has started, right...?"));
            return true;
        }

        sender.sendMessage(Utils.color("&eForcefully beginning the game..."));
        minigame.beginCountdown();
        return true;
    }
}
