package com.mcranked.mgcore.api;

import com.mcranked.mgcore.MGCore;
import com.mcranked.mgcore.api.format.FormatHelper;
import com.mcranked.mgcore.api.format.Formatter;
import com.mcranked.mgcore.utils.CountdownHelper;
import com.mcranked.mgcore.utils.Utils;
import com.mcranked.mgcore.utils.scoreboard.PlayerScoreboard;
import com.mcranked.mgcore.utils.scoreboard.ScoreboardAPI;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class Minigame {
    protected final List<UUID> players = new ArrayList<>();
    @Getter protected long countdownStart = Long.MIN_VALUE;
    @Getter protected int gameCountdown = Integer.MIN_VALUE;
    @Getter private boolean isDedicated;
    @Getter private boolean isGameStarted;
    @Getter private FormatHelper helper;
    @Getter private CountdownHelper countdownHelper;

    public Minigame() {
        this(true);
    }

    protected Minigame(boolean dedicated) {
        this.isDedicated = dedicated;

        // set up the format helper
        helper = new FormatHelper();
        final Minigame minigame = this;
        helper.getFormatters().addAll(Arrays.asList(new Formatter("game_name") {
            @Override
            public String format() {
                return minigame.getName();
            }
        }, new Formatter("players_count") {
            @Override
            public String format() {
                return String.valueOf(minigame.getPlayers().size());
            }
        }, new Formatter("players_max") {
            @Override
            public String format() {
                return String.valueOf(minigame.getMaxPlayers());
            }
        }, new Formatter("players_status") {
            @Override
            public String format() {
                return minigame.getPlayers().size() < minigame.getMinPlayers() || minigame.getCountdownHelper() == null ?
                        (minigame.getPlayers().size() < minigame.getMinPlayers() ?
                                MGCore.getConf().getString("scoreboard.playerStatus.waitingPlayers") :
                                MGCore.getConf().getString("scoreboard.playerStatus.waitingStart")) :
                        helper.format(MGCore.getConf().getString("scoreboard.playerStatus.countdown"));
            }
        }, new Formatter("seconds") {
            @Override
            public String format() {
                CountdownHelper helper = minigame.getCountdownHelper();
                if (helper == null) {
                    return "-1";
                }
                return String.valueOf(helper.getSeconds());
            }
        }));
    }

    public abstract String getName();

    public void gameStart() {
        // send the start message
        messageAll(MGCore.getConf().getString("startMessage"));
        // set the game start to true
        this.isGameStarted = true;
    }

    public void gameEnd() {
        // make sure ticking doesn't continue
        this.isGameStarted = false;
    }

    public void onPreGameTick() {
        for (Player player : getPlayers()) {
            PlayerScoreboard scoreboard = ScoreboardAPI.scoreboardFor(player);
            scoreboard.setDisplayName(MGCore.getConf().getString("scoreboard.title"));

            // format each line
            List<String> lines = MGCore.getConf().getStringList("scoreboard.preGame");
            lines = lines.stream().map(helper::format).collect(Collectors.toList());
            scoreboard.setLines(lines);

            // show the scoreboard
            scoreboard.show();
        }
    }

    public void onGameTick() {
    }

    public void onEnable() {
    }

    public void onDisable() {
    }

    public void messageAll(String message) {
        getPlayers().forEach(player -> player.sendMessage(Utils.color(message)));
    }

    public void join(Player player) {
        if (players.size() + 1 > getMaxPlayers()) {
            Utils.tickDelay(() -> player.kickPlayer(Utils.color("&cThe game you tried to join is full. Try again later.")));
            return;
        }
        players.add(player.getUniqueId());
        messageAll(String.format(MGCore.getConf().getString("playerJoinMessage"), player.getName(), players.size(), getMaxPlayers()));

        // start the game countdown automatically when the player min
        // has been passed
        if (players.size() >= getMinPlayers() && MGCore.getConf().getBoolean("startAutomatically"))
            beginCountdown();
    }

    public void quit(Player player) {
        if (!players.contains(player.getUniqueId())) {
            return; // you cannot quit what you're not in
        }
        players.remove(player.getUniqueId());
        messageAll(String.format(MGCore.getConf().getString("playerQuitMessage"), player.getName(), players.size(), getMaxPlayers()));
        // cancel countdown if one is running
        if (countdownStart != Long.MIN_VALUE && countdownHelper != null && players.size() < getMinPlayers()) {
            cancelCountdown();
            messageAll(MGCore.getConf().getString("gameCountdown.cancelled"));
        }
    }

    public int getMinPlayers() {
        return 2;
    }

    public int getMaxPlayers() {
        return 6;
    }

    public void beginCountdown() {
        // don't start a new countdown if one is currently running
        if (Bukkit.getScheduler().isCurrentlyRunning(gameCountdown)) {
            return;
        }
        // save the task's id so canceling is easier later
        // also save the helper
        countdownHelper = new CountdownHelper(this, MGCore.getConf().getInt("gameCountdown.start"));
        gameCountdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(MGCore.getInstance(),
                countdownHelper, 20L, 20L);
    }

    public void cancelCountdown() {
        // cancel the running countdown
        // and reset the gameCountdown value
        Bukkit.getScheduler().cancelTask(gameCountdown);
        gameCountdown = Integer.MIN_VALUE;

        countdownHelper = null;
    }

    public List<Player> getPlayers() {
        return players.stream().map(Bukkit::getPlayer).collect(Collectors.toList());
    }
}
