package com.mcranked.mgcore.api.format;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class FormatHelper {
    @Getter private final List<Formatter> formatters = new ArrayList<>();

    public String format(String input) {
        for (Formatter formatter : formatters) {
            String formatNamespace = String.format("%%%s%%", formatter.getName());
            if (!input.contains(formatNamespace)) {
                continue;
            }
            input = input.replace(formatNamespace, formatter.format());
        }
        return input;
    }
}
