package com.mcranked.mgcore.api.format;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public abstract class Formatter {
    @Getter @Setter private String name;

    public abstract String format();
}
